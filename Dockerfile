FROM centos:7

#Install rpm
RUN rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

#Install PHP
RUN yum install -y php70w php70w-cli php70w-opcache php70w-pear php70w-devel php70w-pdo php70w-bcmath \
    php70w-dom php70w-eaccelerator php70w-gd php70w-imap php70w-intl \
    php70w-mbstring php70w-mcrypt php70w-mysql php70w-posix php70w-soap \
    php70w-tidy php70w-xmlrpc php70w-pecl-redis php70w-pecl-xdebug php70w-pecl-mongodb php70w-ldap

#Install httpd server
RUN yum install -y httpd

#Run httpd server
CMD ["/usr/sbin/httpd", "-DFOREGROUND"]

WORKDIR /var/www
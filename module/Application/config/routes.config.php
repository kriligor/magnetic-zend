<?php

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [
    'home' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/',
            'defaults' => [
                'controller' => Controller\IndexController::class,
                'action' => 'index',
            ],
        ],
    ],
    'index' => [
        'type' => Segment::class,
        'options' => [
            'route' => '[/:action]',
            'defaults' => [
                'controller' => Controller\IndexController::class,
                'action' => 'index'
            ],
        ],
    ],
    'user' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/user[/:action][/:key]',
            'defaults' => [
                'controller' => Controller\UserController::class,
                'action' => 'login'
            ],
        ],
        'constraints' => [
            'action' => '(?!show)'
        ]
    ],
    'dashboard' => [
        'type' => Segment::class,
        'options' => [
            'route' => '/dashboard[/:action][/:id]',
            'defaults' => [
                'controller' => Controller\DashboardController::class,
                'action' => 'index'
            ],
        ],
    ],
];
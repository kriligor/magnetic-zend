<?php

namespace Application\Controller;

use Application\Model\User;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class DashboardController
 *
 * @package Application\Controller
 */
class DashboardController extends AbstractController
{

    /**
     * Index action
     * @return ViewModel
     */
    public function indexAction()
    {
        $user = $this->getEntityManager()
            ->getRepository(User::class)
            ->findOneBy(
                [
                    'id' => $this->getUser()->getId()
                ]
            );

        return new ViewModel(
            [
                'user' => $user
            ]
        );
    }

    /**
     * Reports action
     * @return ViewModel
     */
    public function reportsAction()
    {
        return new ViewModel;
    }

    /**
     * Configuration action
     * @return ViewModel
     */
    public function configurationAction()
    {
        return new ViewModel;
    }

    public function usersAction()
    {
        $users = $this->getEntityManager()
            ->getRepository(User::class)
            ->findAll();

        return new ViewModel(
            [
                'users' => $users
            ]
        );
    }

    /**
     * Edit action
     * @return JsonModel|ViewModel
     */
    public function editAction()
    {
        $id = $this->params('id');
        if ($id !== null && true == $this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()) {
            $user = $this->getEntityManager()
                ->getRepository(User::class)
                ->findOneBy(
                    [
                        'id' => $id
                    ]
                );

            if ($user !== null) {
                $user->setEmail($this->getRequest()->getPost('email'));
                $user->setName($this->getRequest()->getPost('name'));
                $user->setRole($this->getRequest()->getPost('role'));
                $this->getEntityManager()->persist($user);
                $this->getEntityManager()->flush();

                return new JsonModel(
                    [
                        'redirect' => $this->url()
                            ->fromRoute(
                                'dashboard',
                                [
                                    'action' => 'users'
                                ]
                            ),
                    ]
                );
            }

            return $this->notFoundAction();

        } else {
            if (null !== $id) {
                $user = $this->getEntityManager()
                    ->getRepository(User::class)
                    ->findOneBy(['id' => $id]);

                if (null !== $user) {
                    $view = new ViewModel(
                        [
                            'user' => $user,
                            'action'  => $this->url()->fromRoute('dashboard', ['action' => 'edit'])
                        ]
                    );
                    $view->setTemplate('application/dashboard/edit');

                    return $view;
                }
            }

            return $this->notFoundAction();
        }
    }
}

<?php

namespace Application\Controller;

use Application\Form\Login;
use Application\Form\Register;
use Application\Model\User;
use Zend\Http\Request;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class UserController
 * @package Application\Controller
 */
class UserController extends AbstractController
{

    public function restrictLoggedIn()
    {
        if ($this->getUser() !== null) {
            $this->redirect()->toRoute('home');
        }
    }

    /**
     * Login action
     * @return ViewModel
     */
    public function loginAction()
    {
        $this->restrictLoggedIn();

        /** @var Request $request */
        $request = $this->getRequest();

        if (true === $request->isPost() && true === $request->isXmlHttpRequest()) {

            $json = new JsonModel();

            $form = new Login(
                [
                    'email' => $request->getPost('email'),
                ]
            );

            $data = $request->getPost();

            if ($form->setData($data)->isValid() === true) {
                $json->setVariable('redirect', $this->url()->fromRoute('home'));
                $this->getAuth()->getStorage()->write($form->getIdentity());
            } else {
                $json->setVariable('errors', $form->getMessages());
            }

            return $json;
        }

        return new ViewModel();
    }

    /**
     * @return ViewModel|array
     */
    public function registerAction()
    {
        $this->restrictLoggedIn();

        /** @var Request $request */
        $request = $this->getRequest();

        if (true === $request->isPost() && true === $request->isXmlHttpRequest()) {

            $json = new JsonModel();

            $form = new Register(
                [
                    'email' => $request->getPost('email'),
                    'entityManager' => $this->getEntityManager()
                ]
            );

            $data = $request->getPost();

            if ($form->setData($data)->isValid() === true) {

                $user = new User();
                $user->setEmail($form->get('email')->getValue());
                $user->setName($form->get('name')->getValue());
                $user->setPassword(User::hashPassword($form->get('password')->getValue()));
                $user->setRole(User::ROLE_EMPLOYEE);

                $this->getEntityManager()->persist($user);
                $this->getEntityManager()->flush();

                $json->setVariables(
                    [
                        'redirect' => $this->url()->fromRoute('home'),
                        'message' => 'Successfully registered'
                    ]
                );
            } else {
                $json->setVariable('errors', $form->getMessages());
            }

            return $json;
        }

        return new ViewModel();
    }

    /**
     * @return void
     */
    public function logoutAction()
    {
        $this->getAuth()
            ->getStorage()
            ->clear();

        $this->redirect()->toRoute('user', ['action' => 'login']);
    }
    
}